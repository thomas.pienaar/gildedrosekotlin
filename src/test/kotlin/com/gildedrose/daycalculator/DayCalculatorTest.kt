package com.gildedrose.daycalculator

import com.gildedrose.Item
import org.junit.Assert
import org.junit.Test

class DayCalculatorTest {

    @Test
    fun `Salfuras item never exprires`(){
        val sulfurasItem = Item(NAME_SULFURAS, 0, 80)

        val sulfurasPassingDayItemCalculator = DayCalculator.getItemCalculator(sulfurasItem.name)
        sulfurasPassingDayItemCalculator.calculatePassingDay(sulfurasItem)

        Assert.assertEquals(0, sulfurasItem.sellIn)
        Assert.assertEquals(80, sulfurasItem.quality)

    }

    @Test
    fun `Aged brie matures`() {
        val agedBrieItem = Item(NAME_AGED_BRIE, 2, 0)

        val agedBriesPassingDayItemCalculator = DayCalculator.getItemCalculator(agedBrieItem.name)
        agedBriesPassingDayItemCalculator.calculatePassingDay(agedBrieItem)

        Assert.assertEquals(1, agedBrieItem.sellIn)
        Assert.assertEquals(1, agedBrieItem.quality)
    }

    @Test
    fun `Expired brie matures twice as fast`() {
        val agedBrieItem = Item(NAME_AGED_BRIE, 0, 10)

        val agedBriesPassingDayItemCalculator = DayCalculator.getItemCalculator(agedBrieItem.name)
        agedBriesPassingDayItemCalculator.calculatePassingDay(agedBrieItem)

        Assert.assertEquals(-1, agedBrieItem.sellIn)
        Assert.assertEquals(12, agedBrieItem.quality)
    }

    @Test
    fun `Aged brie upper limit`() {
        val agedBrieItem = Item(NAME_AGED_BRIE, 0, 50)

        val agedBriesPassingDayItemCalculator = DayCalculator.getItemCalculator(agedBrieItem.name)
        agedBriesPassingDayItemCalculator.calculatePassingDay(agedBrieItem)

        Assert.assertEquals(-1, agedBrieItem.sellIn)
        Assert.assertEquals(50, agedBrieItem.quality)
    }

    @Test
    fun `Backstage pass increases in value`() {
        val backstagePassItem = Item(NAME_BACKSTAGE_PASS, 20, 1)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(19, backstagePassItem.sellIn)
        Assert.assertEquals(2, backstagePassItem.quality)
    }

    @Test
    fun `Backstage pass double increases in value within 10 days`() {
        val backstagePassItem = Item(NAME_BACKSTAGE_PASS, 10, 1)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(9, backstagePassItem.sellIn)
        Assert.assertEquals(3, backstagePassItem.quality)
    }

    @Test
    fun `Backstage pass triple increase value within 5 days`() {
        val backstagePassItem = Item(NAME_BACKSTAGE_PASS, 5, 1)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(4, backstagePassItem.sellIn)
        Assert.assertEquals(4, backstagePassItem.quality)
    }

    @Test
    fun `Backstage pass can only increase to 50 max`() {
        val backstagePassItem = Item(NAME_BACKSTAGE_PASS, 5, 49)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(4, backstagePassItem.sellIn)
        Assert.assertEquals(50, backstagePassItem.quality)
    }

    @Test
    fun `Backstage pass expires after seel in date`() {
        val backstagePassItem = Item(NAME_BACKSTAGE_PASS, 1, 49)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(0, backstagePassItem.sellIn)
        Assert.assertEquals(0, backstagePassItem.quality)
    }

    @Test
    fun `Normal items quality decremented each day`() {
        val backstagePassItem = Item("All other items", 20, 1)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(19, backstagePassItem.sellIn)
        Assert.assertEquals(0, backstagePassItem.quality)
    }

    @Test
    fun `Normal item quality lower limit 0`() {
        val backstagePassItem = Item("All other items", 20, 0)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(19, backstagePassItem.sellIn)
        Assert.assertEquals(0, backstagePassItem.quality)
    }

    @Test
    fun `Conjured items quality decrease twice as fast`() {
        val backstagePassItem = Item(NAME_CONJURED, 20, 2)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(19, backstagePassItem.sellIn)
        Assert.assertEquals(0, backstagePassItem.quality)
    }

    @Test
    fun `Conjured items quality lower limit zero` () {
        val backstagePassItem = Item(NAME_CONJURED, 20, 0)

        val backstagePassPassingDayItemCalculator = DayCalculator.getItemCalculator(backstagePassItem.name)
        backstagePassPassingDayItemCalculator.calculatePassingDay(backstagePassItem)

        Assert.assertEquals(19, backstagePassItem.sellIn)
        Assert.assertEquals(0, backstagePassItem.quality)
    }
}