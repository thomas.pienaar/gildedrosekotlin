package com.gildedrose

import org.junit.Assert

fun main(args: Array<String>) {

    val stringBuffer = StringBuffer()

    stringBuffer.append("OMGHAI!\n")

    val items = arrayOf(Item("+5 Dexterity Vest", 10, 20), //
            Item("Aged Brie", 2, 0), //
            Item("Aged Brie", 0, 0), //
            Item("Aged Brie", 0, 50), //
            Item("Elixir of the Mongoose", 5, 7), //
            Item("Elixir of the Mongoose", 5, 0), //
            Item("Sulfuras, Hand of Ragnaros", 0, 80), //
            Item("Sulfuras, Hand of Ragnaros", -1, 80),
            Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            Item("Backstage passes to a TAFKAL80ETC concert", 10, 45),
            Item("Backstage passes to a TAFKAL80ETC concert", 5, 45),
            Item("Backstage passes to a TAFKAL80ETC concert", 5, 50),
            Item("Conjured Mana Cake", 3, 6),
            Item("Conjured Mana Cake", 3, 0))

    val app = GildedRose(items)

    var days = 2
    if (args.size > 0) {
        days = Integer.parseInt(args[0]) + 1
    }

    for (i in 0..days - 1) {
        stringBuffer.append("-------- day $i --------\n")
        stringBuffer.append("name, sellIn, quality\n")
        for (item in items) {
            stringBuffer.append("$item\n")
        }
        println()
        app.updateQuality()
    }
    println(stringBuffer.toString())

    Assert.assertEquals("OMGHAI!\n" +
            "-------- day 0 --------\n" +
            "name, sellIn, quality\n" +
            "Item(name=+5 Dexterity Vest, sellIn=10, quality=20)\n" +
            "Item(name=Aged Brie, sellIn=2, quality=0)\n" +
            "Item(name=Aged Brie, sellIn=0, quality=0)\n" +
            "Item(name=Aged Brie, sellIn=0, quality=50)\n" +
            "Item(name=Elixir of the Mongoose, sellIn=5, quality=7)\n" +
            "Item(name=Elixir of the Mongoose, sellIn=5, quality=0)\n" +
            "Item(name=Sulfuras, Hand of Ragnaros, sellIn=0, quality=80)\n" +
            "Item(name=Sulfuras, Hand of Ragnaros, sellIn=-1, quality=80)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=15, quality=20)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=10, quality=45)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=5, quality=45)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=5, quality=50)\n" +
            "Item(name=Conjured Mana Cake, sellIn=3, quality=6)\n" +
            "Item(name=Conjured Mana Cake, sellIn=3, quality=0)\n" +
            "-------- day 1 --------\n" +
            "name, sellIn, quality\n" +
            "Item(name=+5 Dexterity Vest, sellIn=9, quality=19)\n" +
            "Item(name=Aged Brie, sellIn=1, quality=1)\n" +
            "Item(name=Aged Brie, sellIn=-1, quality=2)\n" +
            "Item(name=Aged Brie, sellIn=-1, quality=50)\n" +
            "Item(name=Elixir of the Mongoose, sellIn=4, quality=6)\n" +
            "Item(name=Elixir of the Mongoose, sellIn=4, quality=0)\n" +
            "Item(name=Sulfuras, Hand of Ragnaros, sellIn=0, quality=80)\n" +
            "Item(name=Sulfuras, Hand of Ragnaros, sellIn=-1, quality=80)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=14, quality=21)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=9, quality=47)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=4, quality=48)\n" +
            "Item(name=Backstage passes to a TAFKAL80ETC concert, sellIn=4, quality=50)\n" +
            "Item(name=Conjured Mana Cake, sellIn=2, quality=4)\n" +
            "Item(name=Conjured Mana Cake, sellIn=2, quality=0)\n", stringBuffer.toString())
}
