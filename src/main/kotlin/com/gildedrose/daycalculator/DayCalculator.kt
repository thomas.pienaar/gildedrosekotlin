package com.gildedrose.daycalculator

import com.gildedrose.Item

const val NAME_SULFURAS = "Sulfuras, Hand of Ragnaros"
const val NAME_AGED_BRIE="Aged Brie"
const val NAME_BACKSTAGE_PASS="Backstage passes to a TAFKAL80ETC concert"
const val NAME_CONJURED="Conjured Mana Cake"

private const val QUALITY_UPPER_LIMIT = 50
private const val QUALITY_LOWER_LIMIT = 0

interface DayCalculator {
    companion object {
        fun getItemCalculator(itemName: String): DayCalculator {
            return when (itemName) {
                NAME_SULFURAS -> SulfurasDayItemCalculator()
                NAME_AGED_BRIE -> AgedBrieDayItemCalculator()
                NAME_BACKSTAGE_PASS -> BackstagePassDayItemCalculator()
                NAME_CONJURED -> ConjuredPassingDayItemCalculator()
                else -> NormalDayItemCalculator()
            }
        }
    }

    fun calculatePassingDay(item: Item)

}

private class SulfurasDayItemCalculator : DayCalculator {
    override fun calculatePassingDay(item: Item) {
        //Do nothing - Sulfuras items stay valid forever.
    }
}

private class AgedBrieDayItemCalculator : DayCalculator {
    private val doubleMatureThreshold=0
    override fun calculatePassingDay(item: Item) {
        item.sellIn-=1
        incrementQuality(item)
        if(item.sellIn< doubleMatureThreshold) incrementQuality(item)
    }
}

private class BackstagePassDayItemCalculator : DayCalculator {
    private val doubleIncreaseThreshold = 10
    private val tripleIncreaseThreshold = 5
    private val expiredThreshold = 0

    override fun calculatePassingDay(item: Item) {
        item.sellIn-=1
        if(item.sellIn<=expiredThreshold)
            item.quality=0
        else {
            incrementQuality(item)
            if (item.sellIn <= doubleIncreaseThreshold) incrementQuality(item)
            if (item.sellIn <= tripleIncreaseThreshold) incrementQuality(item)
        }
    }
}

private class NormalDayItemCalculator : DayCalculator {
    override fun calculatePassingDay(item: Item) {
        item.sellIn-=1
        decrementQuality(item)
    }
}

private class ConjuredPassingDayItemCalculator : DayCalculator {
    override fun calculatePassingDay(item: Item) {
        item.sellIn-=1
        decrementQuality(item)
        decrementQuality(item)
    }
}

private fun decrementQuality(item: Item) {
    if(item.quality > QUALITY_LOWER_LIMIT) item.quality-=1
}

private fun incrementQuality(item: Item) {
    if(item.quality < QUALITY_UPPER_LIMIT) item.quality+=1
}