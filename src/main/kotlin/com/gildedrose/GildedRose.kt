package com.gildedrose

import com.gildedrose.daycalculator.DayCalculator

class GildedRose(var items: Array<Item>) {

    fun updateQuality() {
        for (item in items) {

            val itemCalculator = DayCalculator.getItemCalculator(item.name)
            itemCalculator.calculatePassingDay(item)
        }
    }
}

